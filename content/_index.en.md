---
title: "Open Source Academic Publishing Suite"
date: 2021-02-06T18:58:57+01:00
draft: false
description: "XML-based workflows for media-neutral publishing (e.g. Open Access) without technical expertise using OS-APS"
---

<p class="text-big m0">OS-APS enables XML-based workflows for media-neutral publishing (e.g. Open Access) without technical expertise and cost-intensive XML editing and content management systems. Corporate design can be controlled via existing typesetting templates or in detail with a template development kit.</p>

<p class="text-big">The OS-APS STEMO enhancement project, funded by the BMBF from 09.2023 - 08.2025, is working on a better support for complex formulae, tables and images. Another focus is on the accessible design of the produced publications and the provision of an OS-APS online reader based on JATS/BITS-XML.</p>

<div style="background: ghostwhite; 
            font-size: 20px; 
            padding: 10px; 
            border: 1px solid lightgray; 
            margin: 10px;">
OS-APS can be <a href="/en/demo/">tested live</a>, <a href="https://docs.sciflow.org/self-hosting/">hosted on its own</a> or be extended under the MIT license (<a href="/en/source">source code</a>).
</div>

<div class="center py2 lg-py4 mt4">
    <a href="https://youtu.be/yBRvsjGBzrk" rel="noopener" target="_blank">
    <img src="images/video_preview.png" width="600" height="309" alt="Video preview image" />
    </a>
    <p>
    <small class="center">OS-APS Introduction (Link to Youtube)</small>
    <p>
</div>

<div class="border-top border-bottom py2 lg-py4 mt4">
    <h2 class="mt2 mb1 h3">Project partners</h2>
    <h3 class="mt2 mb1 h4">Current project OS-APS STEMO</h3>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
    <h3 class="mt2 mb1 h4">Former project term OS-APS until 31/03/2023</h3>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//uni-halle.de" target="_blank" rel="noopener">
                <img src="images/mlu-logo.png" alt="Martin-Luther-Universität Halle-Wittenberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
</div>