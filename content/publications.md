# Publikationen

## Artikel

Borchert, C.; Cozatl, R.; Eichler, F.; Hoffmann, A.; Putnings, M. Automatic XML Extraction from Word and Formatting of E-Book Formats: Insight into the Open Source Academic Publishing Suite (OS-APS). Publications 2023, 11, 1. [https://doi.org/10.3390/publications11010001](https://doi.org/10.3390/publications11010001)

Borchert, C., & Hoffmann, A. (2022). Open-Access-Publikationen mit OS-APS medienneutral und mit automatisiertem Corporate Design erstellen: Anforderungserhebung, Schlussfolgerungen für den Publikationsworkflow und Stand der Umsetzung. O-Bib. Das Offene Bibliotheksjournal / Herausgeber VDB, 9(4), 1–19. [https://doi.org/10.5282/o-bib/5859](https://doi.org/10.5282/o-bib/5859)

Putnings, Markus, Borchert, Carsten and Cozatl, Roberto. "Ein Einblick in das BMBF-Projekt Open Source Academic Publishing Suite (OS-APS)" ABI Technik, vol. 42, no. 3, 2022, pp. 166-173. [https://doi.org/10.1515/abitech-2022-0030](https://doi.org/10.1515/abitech-2022-0030)


## Konferenzen und Tagungen

* Enable!-Werkstattgespräch 2021 (vgl. https://enable-oa.org/news/einladung-zum-werkstatt-gespraech-mit-dem-bmbf-projekt-os-aps-open-source-academic-publishing)
* Library Publishing Forum 2021 (vgl. [https://librarypublishing.org/lpf21-lightning-talks/)](https://librarypublishing.org/lpf21-lightning-talks/)
* DINI-Jahrestagung 2021 (vgl. https://dini.de/fileadmin/JT_21/DINI_2021_Poster_OS-APS.pdf)
* Munin Konferenz 2021 (vgl. [https://septentrio.uit.no/index.php/SCS/article/view/6188)](https://septentrio.uit.no/index.php/SCS/article/view/6188)
* Open Access Tage 2021 (vgl. https://doi.org/10.5281/zenodo.5526591)
* Bibliothekskongress 2022 (vgl. https://nbn-resolving.org/urn:nbn:de:0290-opus4-178739)
* PUBMET2022 - The 9th Conference on Scholarly Communication in the Context of Open Science (vgl. [https://pubmet2022.unizd.hr/session/automatic-xml-extraction-from-word-and-formatting-of-e-book-formats-insight-into-the-open-source-academic-publishing-suite-os-aps-project/#)](https://pubmet2022.unizd.hr/session/automatic-xml-extraction-from-word-and-formatting-of-e-book-formats-insight-into-the-open-source-academic-publishing-suite-os-aps-project/#)
* OASPA 2022 Conference (vgl. [https://oaspa.org/wp-content/uploads/2022/09/Hoffman-OASPA_Poster-OS-APS_2022-09-09.pdf)](https://oaspa.org/wp-content/uploads/2022/09/Hoffman-OASPA_Poster-OS-APS_2022-09-09.pdf)
* BiblioCon 2024 (vgl. https://nbn-resolving.org/urn:nbn:de:0290-opus4-189078)
* Open Access Tage 2024 (vgl. https://doi.org/10.5281/zenodo.13898477)
