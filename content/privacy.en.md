---
title: "Data privacy statement"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Data privacy statement for OS-APS"
---

## Introduction

With the following data protection declaration we would like to
which types of your personal data (hereinafter also referred to as \"data") we use
hereinafter also referred to as \"data"), for what purposes and to what extent.
purposes and to what extent. The data protection declaration applies to all processing of
all processing of personal data carried out by us, both in the context of
the provision of our services and, in particular, on our websites, in mobile applications
websites, in mobile applications and within external online
online presences, such as our social media profiles (hereinafter collectively referred to as
collectively referred to as \"Online Offerings").

The terms used are not gender-specific.

Status: February 8, 2021

## Table of contents

- [Introduction](en/privacy#m14)
- [Controller](en/privacy#m3)
- [Overview of processing operations](en/privacy#mOverview)
- [Applicable legal bases](en/privacy#m13)
- [Security measures](en/privacy#m27)
- [Transfer and disclosure of personal data](en/privacy#m25)
- [Data processing in third countries](en/privacy#m24)
- [Use of cookies](en/privacy#m134)
- [Provision of the online offer and web hosting](en/privacy#m225)
- [Blogs and publication media](en/privacy#m104)
- [Contact us](en/privacy#m182)
- [Newsletter and electronic notifications](en/privacy#m17)
- [Web analytics, monitoring and optimization](en/privacy#m263)
- [Presence on social networks (social Media)](en/privacy#m136)
- [Plugins and embedded functions and content](en/privacy#m328)
- [Deletion of data](en/privacy#m12)
- [Modification and updating of the Privacy Policy](en/privacy#m15)
- [Data subject rights](en/privacy#m10)
- [Definitions of terms](en/privacy#m42)

## Person responsible {#m3}

SciFlow GmbH\
Belziger Str. 69-71\
10823 Berlin

Authorized representatives: Frederik Eichler, Dr. Carsten Borchert.

E-mail address: <mail@sciflow.net>.

Imprint: <https://www.os-aps.de/impressum>.

## Overview of processing operations {#mOverview}

The following overview summarizes the types of data processed and
purposes of their processing and refers to the data subjects.
Individuals.

### Types of data processed

- Inventory data (e.g., names, addresses).
- Content data (e.g. entries in online forms).
- Contact data (e.g. e-mail, telephone numbers).
- Meta/communication data (e.g. device information, IP addresses).
- Usage data (e.g. web pages visited, interest in content,
    access times).

### Categories of data subjects.

- Communication partners.
- Users (e.g. website visitors, users of online services).

### Purposes of processing

- Provision of our online offer and user friendliness.
- Conversion measurement (measurement of the effectiveness of
    marketing measures).
- Content Delivery Network (CDN).
- Direct marketing (e.g. via e-mail or postal mail).
- Feedback (e.g. collecting feedback via online form).
- Contact requests and communication.
- Profiling (creation of user profiles).
- Remarketing.
- Reach measurement (e.g. access statistics, identification of
    returning visitors).
- Security measures.
- Tracking (e.g. interest/behavior-based profiling, use of cookies).
    of cookies).
- Provision of contractual services and customer service.
- Administration and response to inquiries.

### Applicable legal basis {#m13}

In the following, we share the legal bases of the
Data Protection Regulation (DSGVO), on the basis of which we process the
we process personal data. Please note that
in addition to the regulations of the DSGVO the national
data protection regulations in your or our country of residence and domicile may apply.
may apply. Furthermore, should more specific legal bases apply in individual cases, we will inform you of these in the privacy policy.
we will inform you of these in the data protection declaration.

- **Consent (Art. 6 para. 1 p. 1 lit. a. DSGVO)** - The data subject
    person has given his or her consent to the processing of the
    personal data concerning him or her for a specific purpose
    or several specific purposes given.
- **Contractual performance and pre-contractual requests (Art. 6 para. 1 p. 1
    lit. b. DSGVO)** - Processing is necessary for the performance of a
    contract to which the data subject is a party, or for the
    or for the performance of pre-contractual measures, which are
    request of the data subject.
- **Rightful interests (Art. 6 para. 1 p. 1 lit. f. DSGVO)** - The
    processing is necessary to safeguard the legitimate interests of the
    of the controller or of a third party, unless the data subject's
    Interests or fundamental rights and freedoms of the data subject
    Person which require the protection of personal data,
    override.

**National data protection regulations in Germany**: In addition to the
data protection regulations of the General Data Protection Regulation, national
data protection regulations in Germany. These include in particular
the Act on Protection against Misuse of Personal Data in Data
Data Processing (Federal Data Protection Act -- BDSG). The BDSG contains
special regulations on the right to information, the right to deletion
deletion, the right of objection, the processing of special categories of
categories of personal data, processing for other purposes and the
and automated decision-making in individual cases, including profiling.
including profiling. Furthermore, it regulates data processing
for purposes of the employment relationship (Section 26 BDSG), in particular with regard to
with regard to the establishment, performance or termination of employment
employment relationships as well as the consent of employees.
In addition, the data protection laws of the individual federal states may apply.
may apply.

## Security measures {#m27}

We take security measures in accordance with the legal requirements
taking into account the state of the art, implementation costs and the type
the nature, scope, circumstances and purposes of the processing, as well as the different
the different probabilities of occurrence and the extent of the threat to the rights and
the rights and freedoms of natural persons, appropriate technical and
technical and organizational measures in order to ensure a level of protection
appropriate level of protection.

The measures shall include, in particular, ensuring the confidentiality,
integrity and availability of data by controlling physical and electronic access to the data
electronic access to the data as well as the access, input, transmission and
access, input, disclosure, ensuring availability, and segregation of data.
and their separation. Furthermore, we have established procedures that
the exercise of data subjects' rights, the deletion of data, and
responses to data compromise. Furthermore
we take the protection of personal data into account as early as the
selection of hardware, software and processes, in accordance with the principle of data
in accordance with the principle of data protection, through technology design and
through data protection-friendly default settings.

**SSL encryption (https): To protect your data transmitted via our online
via our online offer, we use SSL encryption.
You can recognize such encrypted connections by the prefix https://
in the address line of your browser.

## Transmission and disclosure of personal data {#m25}

In the course of our processing of personal data, it happens,
that the data is transferred to other bodies, companies, legally independent
or persons or that the data is disclosed to them.
disclosed to them. The recipients of this data may include, for example.
payment institutions in the context of payment transactions, IT service providers or
service providers or providers of services and content that are integrated into a website.
are integrated into a website. In such cases, we observe the
legal requirements and, in particular, conclude appropriate
contracts or agreements with the recipients of your data that serve to protect your data.
recipients of your data.

## Data processing in third countries {#m24}

If we process data in a third country (i.e., outside the European
Union (EU), the European Economic Area (EEA)) or where we process
the processing takes place in the context of the use of third-party services or
the disclosure or transfer of data to other persons, entities or companies, this will
or companies, this shall only take place in accordance with the
legal requirements.

Subject to express consent or contractually or legally required
or legally required transfer, we process the data or have it processed
data only in third countries with a recognized level of data protection,
contractual obligation through so-called standard protection clauses of the
EU Commission, in the presence of certifications or binding internal data protection
internal data protection regulations (Art. 44 to 49 DSGVO,
EU Commission information page:
<https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de>).

## Use of cookies {#m134}

Cookies are text files that contain data from visited websites or domains
domains and are stored by a browser on the user's computer.
stored by a browser on the user's computer. The primary purpose of a cookie is to
information about a user during or after his or her visit
within an online offer. The stored
information may include, for example, the language settings on a web page, the
login status, a shopping cart or the place where a video was watched.
was watched, belong. We further include in the term cookies other
Technologies that perform the same functions as cookies (for example,
when user information is stored on the basis of pseudonymous online identifiers, also known as "cookies".
also referred to as \"user IDs\").

**The following cookie types and functions are distinguished:**.

- Temporary cookies (also: session cookies or
    Session cookies):** Temporary cookies are deleted at the latest,
    after a user has left an online offer and closed his browser.
    has closed his browser.
- Permanent cookies remain stored even after the browser is closed.
    stored even after the browser is closed. For example, the
    login status can be stored or preferred content can be displayed directly
    be displayed directly when the user visits a website again. Likewise
    the interests of users, which are used for reach measurement or marketing
    marketing purposes, can be stored in such a cookie.
    be stored.
- **First-party cookies:** First-party cookies are set by ourselves.
    set.
- **Third-party cookies (also: third-party cookies)**:
    Third-party cookies are mainly used by advertisers (so-called.
    Third parties) to process user information.
- **Necessary (also: essential or absolutely necessary).
    cookies:** Cookies can be absolutely necessary for the operation of a website on the one hand
    necessary for the operation of a website (e.g. to store logins or other user
    user input or for security reasons).
- **Statistics, marketing and personalization cookies**: Furthermore
    cookies are also generally used in the context of range measurement and when
    and when the interests of a user or his or her behavior (e.g. viewing
    behavior (e.g. viewing of certain content, use of functions, etc.) on individual
    etc.) on individual websites are stored in a user profile.
    are stored. Such profiles are used, for example, to display content to users that
    that correspond to their potential interests. This
    process is also referred to as \"tracking\", i.e., tracking of the potential
    potential interests of the users. Insofar as we use cookies
    or "tracking" technologies, we inform you separately in our data protection
    separately in our data privacy statement or in the context of obtaining consent.
    Obtaining consent.

**Notes on legal basis:** On which legal basis we process your
we process your personal data with the help of cookies depends on whether
Whether we ask you for consent. If this is the case and you
consent to the use of cookies, the legal basis for the processing of your
Processing of your data is the declared consent. Otherwise
the data processed with the help of cookies is processed on the basis of our
legitimate interests (e.g. in a business operation of our online offer and its
operation of our online offer and its improvement) or, if the use of cookies is
the use of cookies is necessary in order to fulfill our contractual
obligations to fulfill.

**Duration of storage:** Unless we provide you with explicit information on the
storage period of permanent cookies (e.g. in the context of a so-called cookie opt-in).
cookie opt-in), please assume that the storage period can be up to two years.
can be up to two years.

**General information on revocation and objection (opt-out):** Depending on
depending on whether the processing is based on consent or legal
legal permission, you have the possibility at any time,
revoke your consent or object to the processing of your data by cookie
data by cookie technologies at any time (collectively referred to as the
\"opt-out\"). You can initially declare your objection by means of
settings of your browser, e.g. by deactivating the use of cookies (whereby this
cookies (although this may also restrict the functionality of our online offer).
of our online offer may be restricted). An objection
against the use of cookies for online marketing purposes can also be made
by means of a variety of services, especially in the case of tracking,
via the websites <https://optout.aboutads.info> and
<https://www.youronlinechoices.com/> can be declared. In addition, you can
receive further objection notices in the context of the information on the used
Service providers and cookies used.

**Processing of cookie data on the basis of consent**: We
use a cookie consent management procedure, in the context of which the
the consent of users to the use of cookies, or to the use of the data
the processing and providers mentioned in the cookie consent management procedure.
and providers mentioned in the cookie consent management procedure.
and revoked by the users. The declaration of consent is stored
stored so that it does not have to be repeated and the consent can be
to be able to prove consent in accordance with the legal obligation.
legal obligation. The storage can take place on the server side and/or in a cookie.
(so-called opt-in cookie, or with the aid of comparable technologies) in order to
technologies), in order to be able to assign the consent to a user or his device.
to be able to assign the consent. Subject to individual information on the providers of
cookie management services, the following information applies: The duration of the
The storage of consent can last up to two years. In this
a pseudonymous user identifier is created and linked to the time of the
of the consent, information on the scope of the consent (e.g. which
categories of cookies and/or service providers) as well as the browser,
System and end device used are stored.

- **Types of data processed:** Usage data (e.g. websites visited,
    interest in content, access times), meta/communication data
    (e.g. device information, IP addresses).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Legal basis:** Consent (Art. 6 para. 1 p. 1 lit. a.
    DSGVO), Legitimate Interests (Art. 6 para. 1 p. 1 lit. f. DSGVO).

## Provision of the online offer and web hosting {#m225}

In order to be able to provide our online offer securely and efficiently,
we make use of the services of one or more web hosting providers
from whose servers (or servers managed by them) the online offer can be accessed.
Online Offer can be accessed. For these purposes we may use
infrastructure and platform services, computing capacity
storage space and database services as well as security services and
technical maintenance services.

The data processed in the context of providing the hosting service may include all data
may include all data relating to the users of our online offer, which is
users of our online service, which is collected in the course of use and communication.
accrue. This regularly includes the IP address, which is necessary
to be able to deliver the contents of online offers to browsers, and
all entries made within our online offer or from websites.
entries.

**Collection of access data and log files**: We ourselves (or our
web hosting provider) collect data about each access to the server
(so-called server log files). The server log files may include the address
the address and name of the web pages and files accessed, the date and time of the
transferred data volume, notification of successful retrieval,
browser type and version, the operating system of the user, referrer URL
(the previously visited page) and, as a rule, IP addresses and the
requesting provider.

The server log files may be used for security purposes, for example to
purposes, e.g., to avoid overloading the servers (especially in the case of
in the case of abusive attacks, so-called DDoS attacks) and
secondly, to ensure the utilization of the servers and their stability.
ensure.

**Content Delivery Network**: We use a
\"Content-Delivery-Network\" (CDN). A CDN is a service with the
with the help of which the content of an online offer, in particular large
media files, such as graphics or program scripts, with the help of regionally
regionally distributed servers connected via the Internet.
and more securely with the help of regionally distributed servers connected via the Internet.

Services used and service providers:

1&1 IONOS: Hosting platform for websites; service provider: 1&1 IONOS SE,
Elgendorfer Str. 57, 56410 Montabaur, Germany; Website:
<https://www.ionos.de>; privacy policy:
<https://www.ionos.de/terms-gtc/terms-privacy>.

Gitlab: development environment and hosting platform for websites: GitLab
Inc. with offices at 268 Bush Street, Suite 350, San Francisco, CA.
94104; website: <https://about.gitlab.com>; Privacy Policy:
<https://about.gitlab.com/privacy/>.

- **Types of data processed:** Content data (e.g., input in
    online forms), usage data (e.g. web pages visited, interest
    in content, access times), meta/communication data (e.g.
    Device information, IP addresses).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Purposes of processing:** Content Delivery Network (CDN).
- **Legal basis:** Legitimate interests (Art. 6 para. 1 p. 1
    lit. f. DSGVO).

## Blogs and publication media {#m104}

We use blogs or comparable means of online communication and
publication (hereinafter \"publication media\"). The data of the readers
are processed for the purposes of the publication medium only insofar
as it is necessary for its presentation and communication between authors and
readers or for security reasons. For the rest
we refer to the information on the processing of visitors to our
of our publication medium within the scope of this data protection notice.

**Comments and Contributions**: When users leave comments or other
contributions, their IP addresses may be stored on the basis of our
legitimate interests. This is done for our
security, in case someone leaves illegal content in comments and posts (insults
content (insults, prohibited political propaganda, etc.).
etc.). In this case, we ourselves can be prosecuted for the comment or contribution
and are therefore interested in the identity of the author.
interested.

Furthermore, we reserve the right, on the basis of our legitimate
interests to process the information provided by users for the purpose of spam detection.

On the same legal basis, we reserve the right, in the case of surveys
the IP addresses of users for the duration of surveys and to use cookies to
to avoid multiple votes.

The personal information provided in the context of the comments and contributions
contact and website information as well as the content of the comments and
information is permanently stored by us until the user objects.
permanently stored.

- **Types of data processed:** Inventory data (e.g. names, addresses),
    contact data (e.g. e-mail, telephone numbers), content data (e.g.
    entries in online forms), usage data (e.g. web pages visited
    websites, interest in content, access times),
    Meta/communication data (e.g., device information, IP addresses).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Purposes of processing:** Provision of contractual services and.
    customer service, feedback (e.g., collecting feedback via online
    online form), security measures, administration and response to
    of requests.
- **Legal basis:** Contract performance and pre-contractual requests.
    (Art. 6 para. 1 p. 1 lit. b. DSGVO), Legitimate Interests (Art. 6.
    Abs. 1 S. 1 lit. f. DSGVO).

## Contacting {#m182}

When contacting us (e.g. via contact form, email,
telephone or via social media), the information provided by the inquiring
processed, insofar as this is necessary to respond to the contact requests
and any measures requested.

The response to the contact requests in the context of contractual or
contractual or pre-contractual relations is carried out for the fulfillment of our contractual
contractual obligations or to respond to (pre)contractual inquiries and, in all other
and, moreover, on the basis of the legitimate interests in answering
the inquiries.

- **Types of data processed:** Inventory data (e.g. names, addresses),
    contact data (e.g. e-mail, telephone numbers), content data (e.g.
    Entries in online forms).
- **Data subjects:** Communication partners.
- **Purposes of processing:** Contact requests and communication.
- **Legal basis:** Contract performance and pre-contractual inquiries.
    (Art. 6 para. 1 p. 1 lit. b. DSGVO), Legitimate Interests (Art. 6.
    Abs. 1 S. 1 lit. f. DSGVO).

## Newsletters and electronic notifications {#m17}

We send newsletters, emails and other electronic
notifications (hereinafter referred to as \"newsletters") only with the consent
of the recipients or a legal permission. If in the context of a
newsletter, the content of the newsletter shall be deemed to be the consent of the user.
they are decisive for the consent of the users. For the rest, our newsletters contain
our newsletters contain information about our services and us.

In order to register for our newsletters, it is sufficient in principle
if you provide your e-mail address. However, we may ask you
a name, for the purpose of personal address in the newsletter, or further
information, if this is necessary for the purpose of the newsletter,
to make.

**Double-Opt-In-Procedure:** The registration for our newsletter takes place
in a so-called double opt-in procedure. I.e., you
registration, you will receive an e-mail asking you to confirm your registration.
confirmation of your registration. This confirmation is necessary so that
no one can register with other e-mail addresses. The registrations
to the newsletter are logged in order to prove the registration process in accordance with
the legal requirements. This includes the
the time of registration and confirmation as well as the IP address.
IP address. Likewise, changes to your data stored with the
shipping service provider will be logged.

**Deletion and restriction of processing:** We may keep the
unsubscribed email addresses for up to three years based on our
legitimate interests, before deleting them in order to be able to prove
be able to prove consent formerly given. The processing
of this data is limited to the purpose of a possible defense of claims.
limited. An individual request for deletion is possible at any time,
provided that the former existence of consent is confirmed at the same time.
is confirmed. In the case of obligations for the permanent observance of objections
we reserve the right to store the e-mail address solely for this purpose in a
in a block list for this purpose alone (so-called \"block list\").

The logging of the registration process takes place on the basis of our
legitimate interests for the purpose of proving that the registration process has been
process. If we commission a service provider to send e-mails, this is done on the basis of our
this is done on the basis of our legitimate interests in an efficient and secure
in an efficient and secure dispatch system.

**Notes on legal basis:** The newsletter is sent on the
based on the consent of the recipient or, if consent is not required, on our
consent is not required, on the basis of our legitimate interests in direct
direct marketing, if and insofar as this is permitted by law, e.g. in the case of
Existing customer advertising, is permitted. Insofar as we commission a service provider with
with the dispatch of e-mails, this is done on the basis of our legitimate
of our legitimate interests. The registration process is recorded on
recorded on the basis of our legitimate interests, in order to
prove that it has been carried out in accordance with the law.
has been carried out.

Content: Information about us, our services, promotions and
Offers.

**Measurement of opening and click-through rates**: The newsletters contain a
so-called \"web-beacon", i.e., a pixel-sized file that is sent to our server when the
the newsletter is opened by our server, or, if we use a shipping
service provider, from its server when the newsletter is opened. In the
technical information, such as information about the browser and your system, as
information about the browser and your system, as well as your IP address and the
the time of the retrieval.

This information is used for the technical improvement of our
newsletter on the basis of the technical data or the target groups and their
reading behavior on the basis of their retrieval locations (which can be determined by means of the IP address) or the
can be determined) or the access times. This analysis
also includes the determination of whether the newsletters are opened
are opened, when they are opened and which links are clicked. This
information is assigned to the individual newsletter recipients and stored
stored in their profiles until they are deleted. The evaluations
help us to identify the reading habits of our users and to adapt our
and to adapt our content to them or to send different content
according to the interests of our users.

The measurement of open rates and click rates as well as the storage of the
the measurement results in the profiles of the users and their further
The measurement of opening rates and click rates and the storage of the measurement results in the profiles of the users as well as their further processing are based on the consent of the users.

A separate revocation of the performance measurement is unfortunately not possible.
In this case, the entire newsletter subscription must be cancelled or objected to.
be contradicted. In this case, the stored
profile information will be deleted.

- **Types of data processed:** Inventory data (e.g. names, addresses),
    Contact data (e.g. e-mail, telephone numbers),
    Meta/communication data (e.g. device information, IP addresses),
    Usage data (e.g. websites visited, interest in content,
    access times).
- **Data subjects:** Communication partners.
- **Purposes of processing:** Direct marketing (e.g., via e-mail or
    postal mail).
- **Legal basis:** Consent (Art. 6 para. 1 p. 1 lit. a.
    DSGVO), Legitimate Interests (Art. 6 para. 1 p. 1 lit. f. DSGVO).
- **Opt-Out:** You can cancel the receipt of our newsletter at any time.
    of our newsletter at any time, i.e. revoke your consent, or refuse to
    revoke, or object to further receipt. You will find a link to
    you will find a link to unsubscribe from the newsletter either at the end of each
    newsletter or you can otherwise use one of the above
    contact options, preferably e-mail, for this purpose.

**Services used and service providers:**

- **SendGrid:** Email marketing platform; service provider: SendGrid,
    Inc. 1801 California Street, Suite 500 Denver, Colorado 80202, USA..;
    Website: <https://sendgrid.com>; Privacy Policy:
    <https://sendgrid.com/policies/privacy/>

## Web analytics, monitoring and optimization {#m263}

The web analysis (also referred to as \"reach measurement\") serves to
evaluation of the streams of visitors to our online offering and may
behavior, interests or demographic information about the
visitors, such as age or gender, as pseudonymous values.
include. With the help of the reach analysis, we can recognize, for example, at which
at which time our online offer or its functions or contents are most frequently
are used most frequently or invite reuse. Likewise
we can understand which areas require optimization.

In addition to web analytics, we may also use testing procedures, e.g. in order to
test and optimize different versions of our online offer or its
test and optimize different versions of our online offer or its components.

For these purposes, so-called user profiles may be created and stored in a
file (so-called \"cookies\") or similar procedures may be used for the same
the same purpose may be used. This information may include, for example
content viewed, websites visited and elements used there, and
technical data, such as the browser used, the computer system used and
computer system used as well as information on usage times. If users consent to the
have consented to the collection of their location data, this may also be processed
may also be processed, depending on the provider.

The IP addresses of users are also stored. However,
we use an IP masking procedure (i.e., pseudonymization by shortening the
shortening of the IP address) to protect users. In general, the data collected in the
web analysis, A/B testing and optimization, no clear user data (such as e-mail
(such as e-mail addresses or names) are stored, but rather pseudonyms.
pseudonyms. This means that we as well as the providers of the software used
do not know the actual identity of the users, but only the data stored
information stored in their profiles for the purposes of the respective procedures.

**Notes on legal bases:** If we ask the users for their
consent to the use of the third-party providers, the legal basis for the
legal basis for the processing of data is consent. Otherwise
the users' data is processed on the basis of our legitimate
interests (i.e., interest in efficient, economical and
recipient-friendly services) are processed. In this context
we would also like to draw your attention to the information on the use of cookies in this privacy policy.
this privacy policy.

- **Types of data processed:** Usage data (e.g. web pages visited,
    interest in content, access times), meta/communication data
    (e.g. device information, IP addresses).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Purposes of processing:** Reach measurement (e.g..
    access statistics, recognition of returning visitors), tracking
    (e.g., interest/behavioral profiling, use of cookies), conversion
    cookies), conversion measurement (measurement of the effectiveness of marketing
    Marketing measures), profiling (creation of user profiles).
- **Security measures:** IP masking (pseudonymization of the
    IP address).
- **Legal basis:** Consent (Art. 6 para. 1 p. 1 lit. a.
    DSGVO), Legitimate Interests (Art. 6 para. 1 p. 1 lit. f. DSGVO).

**Services and service providers used:**.

- **Matomo:** The information generated by the cookie about your
    use of this website will only be stored on our server
    and not passed on to third parties; service provider: Web analytics/
    Reach measurement in self-hosting; Website: <https://matomo.org/>;
    Deletion of data: Cookies have a maximum storage period of
    13 months.

## Presence in social networks (social media) {#m136}

We maintain online presences within social networks and
process user data in this context in order to communicate with users who are active
users active there or to offer information about us.

We would like to point out that user data may be processed outside the area of the
of the European Union may be processed. This may result in
risks for the user, for example because the enforcement of the rights of the user could be
rights of the users could be made more difficult.

Furthermore, the data of users within social networks is usually used for
usually processed for market research and advertising purposes. Thus
e.g., user profiles can be created on the basis of the user behavior and the resulting
of the users can be used to create usage profiles. The usage profiles can
in turn be used, for example, to place advertisements within and outside the
and outside the networks that presumably correspond to the interests of the users.
correspond to the interests of the users. For these purposes, cookies are usually stored on the user's
computers, in which the usage behavior and interests of the users are stored.
interests of the users are stored. Furthermore, the
profiles may also include data independent of the devices used by the users (in particular
devices used by the users (especially if the users are members of the respective
respective platforms and are logged in to them).

For a detailed presentation of the respective forms of processing and
the options to object (opt-out), we refer you to the
privacy statements and information provided by the operators of the respective
networks.

Also in the case of requests for information and the assertion of data
rights, we would like to point out that these can be asserted most effectively with the providers.
can be asserted with the providers. Only the providers have
access to user data and can take appropriate measures and provide information directly.
take appropriate measures and provide information. Should you nevertheless require
you need help, you can contact us.

- **Types of data processed:** Inventory data (e.g. names, addresses),
    contact data (e.g. e-mail, telephone numbers), content data (e.g.
    entries in online forms), usage data (e.g. web pages visited
    websites, interest in content, access times),
    Meta/communication data (e.g., device information, IP addresses).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Purposes of processing:** Contact requests and communication,
    Tracking (e.g. interest/behavioral profiling, use of cookies), remarketing.
    of cookies), remarketing.
- **Legal basis:** Legitimate interests (Art. 6 para. 1 p. 1
    lit. f. DSGVO).

**Services used and service providers:**

- **Twitter:** Social network; service provider: Twitter
    International Company, One Cumberland Place, Fenian Street, Dublin 2.
    D02 AX07, Ireland; parent company: Twitter Inc, 1355 Market
    Street, Suite 900, San Francisco, CA 94103, USA;
    Privacy Policy: <https://twitter.com/de/privacy>,
    (settings) <https://twitter.com/personalization>.

## Plugins and embedded functions and content {#m328}

We embed functional and content elements in our online offer,
from the servers of their respective providers (hereinafter referred to as
as \"third-party providers"). These may be, for example
graphics, videos or social media buttons as well as contributions
(hereinafter uniformly referred to as \"content").

The integration always requires that the third-party providers of this content
the IP address of the user, as without the IP address they would not be able to send the
would not be able to send the content to their browser. The IP address is therefore
necessary for the presentation of these contents or functions. We
endeavor to use only such content, whose respective providers
use the IP address only for the delivery of the content.
Third-party providers may also use so-called pixel tags (invisible graphics,
also referred to as \"web beacons\") for statistical or marketing purposes.
marketing purposes. Through the \"pixel tags\" can be
information, such as visitor traffic on the pages of this website,
be evaluated. The pseudonymous information can also be stored in
stored in cookies on the user's device and may contain, among other things
technical information about the browser and operating system, referring
referring web pages, the time of the visit and other information about the use of our
use of our online offer and may also be linked with such information
information from other sources.

**Notes on legal basis:** If we ask the users for their
consent to the use of the third-party providers, the legal basis for the
legal basis for the processing of data is consent. Otherwise
the users' data is processed on the basis of our legitimate
interests (i.e., interest in efficient, economical and
recipient-friendly services) are processed. In this context
we would also like to draw your attention to the information on the use of cookies in this privacy policy.
this privacy policy.

- **Types of data processed:** Usage data (e.g. web pages visited,
    interest in content, access times), meta/communication data
    (e.g. device information, IP addresses), inventory data (e.g. names,
    addresses), contact data (e.g., e-mail, telephone numbers), content data
    (e.g., entries in online forms).
- **Data subjects:** Users (e.g., website visitors, users of
    Online Services).
- **Purposes of the processing:** Provision of our online offer
    and user-friendliness, provision of contractual services and
    customer service, security measures, administration and response to
    requests.
- **Legal basis:** Consent (Art. 6 para. 1 p. 1 lit. a.
    DSGVO), contract performance and pre-contractual inquiries (Art. 6 para.
    1 S. 1 lit. b. DSGVO), Legitimate Interests (Art. 6 para. 1 p. 1.
    lit. f. DSGVO).

**Services used and service providers:**.


- **YouTube Videos:** Video Content; Service Provider: Google Ireland
    Limited, Gordon House, Barrow Street, Dublin 4, Ireland,
    Parent company: Google LLC, 1600 Amphitheatre Parkway, Mountain
    View, CA 94043, USA; website: <https://www.youtube.com>;
    Privacy Policy: <https://policies.google.com/privacy>;
    Opt-out: opt-out plugin:
    <https://tools.google.com/dlpage/gaoptout?hl=de>; settings for
    Display of advertisements:
    <https://adssettings.google.com/authenticated>.

## Deletion of data {#m12}

The data processed by us will be deleted in accordance with the legal requirements
deleted as soon as their consent to the processing is revoked or
Consents are revoked or other permissions cease to apply.
(e.g., if the purpose of the processing of these data has ceased to apply or
they are not required for the purpose).

If the data are not deleted because they are required for other and
and legally permissible purposes, the processing of the data shall be
limited to these purposes. I.e., the data is blocked and not
processed for other purposes. This applies, for example, to data that must be retained for reasons of commercial or tax law.
or tax law reasons or whose storage is required for the assertion, exercise
storage is necessary for the assertion, exercise or defense of
legal claims or to protect the rights of another natural or legal person.
or legal person.

Further information on the deletion of personal data can be found
further in the context of the individual data protection notices of this
Privacy Policy.

## Changes and updates to the privacy policy {#m15}

We ask you to inform yourself regularly about the content of our
Privacy Policy on a regular basis. We adapt the data protection declaration
as soon as changes in the data processing carried out by us make this necessary.
make this necessary. We will inform you as soon as the
changes require an act of cooperation on your part (e.g. consent) or any other
any other individual notification becomes necessary.

If we provide addresses and contact information of companies and
contact information of companies and organizations, please note that the
to note that the addresses may change over time and we ask you to check the information
ask to check the information before contacting them.

## Rights of data subjects {#m10}

As a data subject, you are entitled to various rights under the GDPR, which are
arise in particular from Art. 15 to 21 DSGVO:

- **Right of objection: You have the right to object to the processing of your personal data at any time for reasons
    your particular situation, to object at any time to the processing
    of the personal data concerning you, which is carried out on the basis of Art.
    6 para. 1 lit. e or f DSGVO; this also applies to an objection based on these provisions.
    also applies to profiling based on these provisions.
    If the personal data concerning you is processed for the purpose of
    direct marketing, you have the right at any time to
    object to the processing of the personal data concerning you
    personal data for the purpose of such advertising;
    This also applies to profiling, insofar as it is connected with such
    direct advertising.**
- **Right to revoke consent:** You have the right to revoke your consent at any time.
    Consents at any time to revoke.
- Right of access:** You have the right to request confirmation as to whether or not
    whether the data in question is being processed and to request information
    about these data as well as to further information and copy of the data
    according to the legal requirements.
- Right to rectification:** You have the right, in accordance with the law, to request that
    the right to request the completion of the data concerning you or the
    or the correction of inaccurate data concerning you.
    demand.
- **Right to erasure and restriction of processing:** You have
    in accordance with the legal requirements, the right to request that
    data concerning you be deleted without delay, or alternatively
    alternatively, in accordance with the legal requirements, to request a
    processing of the data.
- **Right to data portability:** You have the right to demand that data
    data that you have provided to us, in accordance with the legal
    in a structured, common and machine-readable format, in accordance with the legal
    machine-readable format, or to request that it be transferred to another responsible
    another responsible party.
- **Complaint to supervisory authority:** You also have the right, in accordance with the
    legal requirements, the right to lodge a complaint with a supervisory authority,
    in particular in the Member State of your habitual
    place of residence, place of work or place of the alleged infringement, if the
    infringement, if you consider that the processing of personal data concerning you has
    processing of personal data relating to you infringes the
    GDPR.

## Definitions of terms {#m42}

This section provides you with an overview of the terms used in this
Privacy Policy. Many of the terms
are taken from the law and defined primarily in Art. 4 of the GDPR. The
legal definitions are binding. The following
explanations, on the other hand, are intended primarily to aid understanding. The
terms are sorted alphabetically.

- **Content Delivery Network (CDN):** A \"Content Delivery Network\"
    (CDN) is a service that can be used to deliver the content of an
    especially large media files, such as graphics or program scripts, using
    scripts with the help of regionally distributed servers connected via the
    and connected via the Internet, can be delivered more quickly and securely.
    can be delivered more securely.
- IP masking refers to a method by which the last octet, i.e., the
    the last octet, i.e., the last two numbers of an IP address, so that the
    IP address is deleted, so that the IP address can no longer be used to
    unique identification of a person. Therefore, the
    IP masking is a means of pseudonymization for
    processing procedures, especially in online marketing
- **Conversion measurement:** Conversion measurement (also known as.
    \"visit action evaluation\") is a procedure with which the effectiveness of marketing
    the effectiveness of marketing measures can be determined.
    For this purpose, a cookie is usually placed on the devices of the users
    within the websites on which the marketing measures are carried out,
    and then retrieved again on the target website.
    For example, this allows us to determine whether the ads we have placed on other websites
    on other websites were successful.
- **Personal Data:** \"Personal Data" means any information that relates to an identified
    information that relates to an identified or
    identifiable natural person (hereinafter \"data subject").
    person"); an identifiable natural person is one who is directly or indirectly
    person directly or indirectly, in particular by reference to an identifier
    to an identifier such as a name, to an identification number, to
    location data, to an online identifier (e.g. cookie), or to one or more special
    or to one or more special characteristics that are an expression of the physical
    expression of the physical, physiological, genetic, psychological, economic, cultural or
    economic, cultural or social identity of this
    natural person.
- **Profiling:** The term \"profiling" is used to describe any type of automated
    processing of personal data which consists in the use of personal data
    that such personal data are used to evaluate certain
    personal aspects relating to a natural person (depending on the type of profiling
    (depending on the type of profiling, this may include information concerning
    age, gender, location data and movement data,
    interaction with websites and their content, shopping behavior,
    social interactions with other individuals) to analyze, evaluate or
    or to predict them (e.g., interests in certain content or products, click behavior
    certain content or products, the click behavior on a
    Website, or location). For profiling purposes, cookies and web beacons are
    cookies and web beacons are often used.
- **Reach measurement:** Reach measurement (also referred to as web
    Analytics) is used to evaluate the flow of visitors to an online
    online offer and can determine the behavior or interests of the visitors
    in certain information, such as the content of web pages,
    content of web pages. With the help of reach analysis, website owners can
    e.g., recognize at what time visitors visit their website and
    what content they are interested in. This enables them to, for example
    better adapt the content of the website to the needs of their visitors.
    the needs of their visitors. For reach analysis purposes, pseudonymous cookies and web beacons are frequently
    cookies and web beacons are often used for the purpose of analyzing
    visitors and thus obtain more precise analyses of the use of an online offer.
    to obtain.
- Remarketing:** The term \"remarketing" or \"retargeting" is used. \"Retargeting" is when, for example,
    for advertising purposes, which products a user was interested in on a website.
    a user has shown interest in on a website, in order to remind the user of these
    products on other websites, e.g. in advertisements, in order to remind the
    Reminder.
- Tracking is when the behavior of users is tracked across multiple online
    behavior can be tracked across several online offerings.
    can be tracked. As a rule, behavioral and interest information is collected with regard to the
    and interest information is stored in cookies or on the servers of the tracking
    stored in cookies or on servers of the tracking technology providers (profiling).
    (so-called profiling). This information can subsequently
    be used, for example, to display advertisements to the users that are likely to
    likely to correspond to their interests.
- **Responsible party:** The \"responsible party" is the natural
    or legal person, authority, institution or other body,
    who alone or jointly with others determines the purposes and means of the
    processing of personal data.
- **Processing:** \"Processing" shall mean any operation or set of operations which is performed
    automated procedures or any series of such procedures in connection with
    series of operations relating to personal data. The
    term is broad and includes virtually any handling of data,
    be it collection, analysis, storage, transmission or deletion.
    or deletion.

[Created with the Datenschutz-Generator.de by Dr. Thomas
Schwenke](privacy.mdhttps://datenschutz-generator.de/?l=de "Legal text by Dr. Schwenke - please click for more information.")

Translated with www.DeepL.com/Translator