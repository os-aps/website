---
title: "Impressum"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Impressum für OS-APS"
---

OS-APS ist ein Gemeinschaftsprojekt von FAU Erlangen-Nürnberg, MLU Halle-Wittenberg und SciFlow ([Kontakt zum Projekt](mailto:mail@os-aps.de)).

Angaben zum Impressum:

SciFlow GmbH \
Belziger Str. 69-71 \
10823 Berlin \
E-Mail: mail@sciflow.net

Telefon: +49 (0) 30 233 210 650

Geschäftsführer: Dr. Carsten Borchert, Frederik Eichler

Registergericht: Amtsgericht Charlottenburg HRB 218297 B

VAT-ID: DE307711343

Verantwortlich nach § 55 Abs. 2 RStV: Dr. Carsten Borchert

Die Europäische Kommission hat eine Plattform zur Online-Streitbeilegung für den Abschluss von Online-Kauf- oder Dienstleistungsverträgen bereitgestellt. Diese finden Sie unter folgendem Link: http://ec.europa.eu/consumers/odr/.

Wir sind zur Teilnahme an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle nicht verpflichtet und bereit.