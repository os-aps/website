# OS-APS source code (Gitlab)

The libraries and templates are both available under a MIT style license.

The most important links are the:

* [Example template repository](https://gitlab.com/os-aps/templates) for you to copy and add to your own instance of OS-APS
* [Documentation](https://docs.sciflow.org) to learn how to configure & build new templates, or host the software. This also includes videos on how to set up your development environment.
* [Library source code (MIT)](https://gitlab.com/sciflow/development) in case you want to modify parts of the libraries themselves or propose changes
* [Documentation and code for](https://github.com/ulb-sachsen-anhalt/ulb-journal-saf) harvesting and export of datasets from the ULB Sachsen-Anhalt's OJS/OMP installations to a DSpace based repository

Last but not least, we do host an [OS-APS instance](/en/demo) ourselves if you do not want to run your own instance.

If you have feedback or questions, please feel free to join our Slack channel (link in the menu above) and start a conversation.