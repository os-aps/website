# Mit OS-APS loslegen

## Live dabei sein

Zu unseren *Community Calls* zeigen wir online was sich neues getan hat und nehmen Feedback und Anregungen auf.
Wir freuen uns auch über mitgebrachte Themen - die gerne kurz im Community Slack ansprechen. <br>
Die Community Calls finden jeden 3. Mittwoch im Monat, 14.00-15.00 statt. <br>

Nächster Community Call:

* 19.März 2025 02:00 Uhr [Zoom](https://fau.zoom-x.de/j/64855273269?pwd=ip6fYNna3yEDCi0EVIp8RiZ0RQLzY0.1)

Alle weiteren Termine befinden sich auf der <a href="/en/demo/">englischen Webseite</a>.

## Selbst ausprobieren

Unsere Demo ermöglicht es eigene Manuscripte hochzuladen und den Export zu fuer eigene Manuskripte zu nutzen.

<a class="mdc-button mdc-button--unelevated" href="https://os-aps.sciflow.net/" rel="noopen" target="_blank">Demo aufrufen</a>
Alle weiteren Links und Informationen befinden sich auf der <a href="/en/demo/">englischen Webseite</a>.

## Demo Video

Um einen schnellen Eindruck vom Import und dem Export als PDF, HTML oder XML zu bekommen, haben wir Demos aufgenommen.

Die Videos befinden sich auf der <a href="/en/demo/">englischen Webseite</a>.
