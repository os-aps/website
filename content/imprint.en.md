---
title: "Imprint"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Imprint for OS-APS"
---

OS-APS is a joint project of FAU Erlangen-Nuremberg, MLU Halle-Wittenberg and SciFlow ([contact the project](mailto:mail@os-aps.de)).

Imprint information:

SciFlow GmbH \
Belziger Str. 69-71 \
10823 Berlin \
E-Mail: mail@sciflow.net

Phone: +49 (0) 30 233 210 650

Managing Directors: Dr. Carsten Borchert, Frederik Eichler

Registry Court: Amtsgericht Charlottenburg HRB 218297 B

VAT-ID: DE307711343

Responsible according to § 55 para. 2 RStV: Dr. Carsten Borchert

The European Commission has provided a platform for online dispute resolution for the conclusion of online purchase or service contracts. This can be found under the following link: http://ec.europa.eu/consumers/odr/.

We are not obliged or willing to participate in dispute resolution proceedings before a consumer arbitration board.