---
title: "Open Source Academic Publishing Suite"
date: 2021-02-06T18:58:57+01:00
draft: false
description: "XML-basierte Workflows zum medienneutralen Publizieren (z.B. Open Access) ohne technische Expertise mit OS-APS"
---

<p class="text-big m0">OS-APS ermöglicht XML-basierte Workflows zum medienneutralen Publizieren (z.B. Open Access) ohne technische Expertise und kostenintensive XML-Redaktions- und Content-Management-Systeme. Das Corporate Design kann über bestehende Satztemplates oder im Detail mit einem Template Development Kit gesteuert werden.</p>

<p class="text-big">Im Ausbauprojekt OS-APS STEMO, gefördert vom BMBF von 09.2023 - 08.2025, wird an einer bessere Unterstützung von komplexen Formeln, Tabellen und Bildern gearbeitet. Ein weiterer Fokus liegt auf der barrierefreien Gestaltung der produzierten Publikationen und der Bereitstellung eines OS-APS Readers auf JATS/BITS-XML Basis.</p>

<div style="background: ghostwhite;
            font-size: 20px;
            padding: 10px;
            border: 1px solid lightgray;
            margin: 10px;">
OS-APS kann <a href="/demo">live getestet</a>, <a href="https://docs.sciflow.org/self-hosting/">selbst gehosted</a> oder selbst unter der MIT Lizenz weiterentwickelt werden (<a href="/en/source">Quellcode</a>).
</div>

<div class="center py2 lg-py4 mt4">
    <a href="https://youtu.be/HGpYO9KXb5w" rel="noopener" target="_blank">
    <img src="images/video_preview.png" width="600" height="309" alt="Video preview image" />
    </a>
    <p>
    <small class="center">OS-APS Einführung (externer Link auf Youtube)</small>
    <p>
</div>

<div class="border-top border-bottom py2 lg-py4">
    <h2 class="mt2 mb1 h3">Projektpartner</h2>
    <h3 class="mt2 mb1 h4">Derzeitige Projektlaufzeit OS-APS STEMO</h3>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
    <h3 class="mt2 mb1 h4">Vormalige Projektlaufzeit OS-APS bis zum 31.03.2023</h3>
    <div class="flex items-center center flex-wrap justify-between mxn2 col-12 lg-col-10 mx-auto">
        <div class="col-4 px2">
            <a href="//fau.de" target="_blank" rel="noopener">
                <img src="images/fau-logo.png" alt="Friedrich-Alexander-Universität Erlangen-Nürnberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//uni-halle.de" target="_blank" rel="noopener">
                <img src="images/mlu-logo.png" alt="Martin-Luther-Universität Halle-Wittenberg logo">
            </a>
        </div>
        <div class="col-4 px2">
            <a href="//sciflow.net" target="_blank" rel="noopener">
                <img src="images/sf-logo-full.svg" alt="SciFlow logo">
            </a>
        </div>
    </div>
</div>
