# Getting started with OS-APS

You can upload your own manuscripts to the demo site or watch a video for the import and export below. Please consider joining our Slack channel if you have any questions or join us for a community call. <br>
Community calls take place every third Wednesday of the month, 2:00 pm - 3:00 pm (CET).

## Join us live

Next community call

* 19.March 2025 02:00 Uhr [Zoom](https://fau.zoom-x.de/j/64855273269?pwd=ip6fYNna3yEDCi0EVIp8RiZ0RQLzY0.1)

## Try it yourself

<a class="mdc-button mdc-button--unelevated" href="https://os-aps.sciflow.net/" rel="noopen" target="_blank">Try it now.</a>

The file being used in the demo is <a download href="/videos/moby-dick-2-chapter1-3.docx">this .docx file</a>.

The files we used in last community call to demonstrate the import of LaTeX files can be found in <a download href="/files/thesis.zip">this .zip file</a>.

## Watch a recorded demo

### Preparing and importing a manuscript

<video width="640" controls="controls" preload="false">
    <source src="/videos/os-aps-demo-1-import.webm" type="video/webm">
    Your browser does not support the video tag.
</video>

<span>Please note that the video does sometimes not play inside Firefox or Safari but will work in Edge, Opera and Chrome.</span>

### Exporting PDF, HTML, and XML with custom settings

<video width="640" controls="controls" preload="false">
    <source src="/videos/os-aps-demo-2-export.webm" type="video/webm">
    Your browser does not support the video tag.
</video>
