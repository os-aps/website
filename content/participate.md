---
title: "Publizieren mit uns gemeinsam einfacher machen"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Gestalten Sie das Projekt mit uns gemeinsam"
---


## **OS-APS STEMO Beirat** {#sec:nmunfiqqe7xg}

Um das Projekt über die gesamte Laufzeit konstruktiv und produktiv zu begleiten und die Software-Entwicklung im Sinne der verlegerischen Community zu priorisieren und zu steuern, wurde ein Beirat gegründet: Der **Beirat** wird zu Projektfragen und -anliegen kontaktiert. Auch erfüllt er eine beratende Funktion bei der strategischen Planung und Ausgestaltung des Projekts.

Der Beirat setzt sich zusammen aus kleinen und mittelgroßen Verlagen, Hochschulverlagen, wissenschaftlichen Bibliotheken, drittmittelgeförderten Projekten und weiteren engagierten Akteuren im Bereich digitales Publizieren und Open Science.

Interesse? Sollten Sie uns im Rahmen des Beirats oder auch abseits davon unterstützen wollen, ist das stets möglich. Sprechen Sie uns einfach an.

Kontakt: [mail@os-aps.de](mailto:mail@os-aps.de)

<div class="mt4"></div>

## **Mitglieder des wissenschaftlichen Beirats** {#sec:uwig46yfsbl7}

<div class="col-10 my4 justify-center mx-auto flex flex-wrap logo-wrapper">
    <img src="/images/partners/de_gruyter_logo.png" alt="de Gruyter" class="m2" />
    <img src="/images/partners/Logo_FID_Philosophie_cmyk-H.jpg" alt="FID Philosophie" class="m2" />
    <img src="/images/partners/Logo_Helmholtz.jpg" alt="Helmholtz Open Science Office" class="m2" />
    <img src="/images/partners/Sikorski_KIT.png" alt="KIT Scientific Publishing" class="m2" />
    <img src="/images/partners/Logo_Universitätsbibliothek_Mannheim.svg" alt="Universitätsbibliothek der Universität Mannheim" class="m2" />
    <img src="/images/partners/Uni_Potsdam_Logo.svg" alt="Universitätsbibliothek Potsdam" class="m2" />
</div>

**De Gruyter Brill**

Franziska Bühring

**USB Köln, FID Philosophie**

Yannik Hampf

**Helmholtz Open Science Office, Helmholtz-Gemeinschaft**

Lea Maria Ferguson

**Hochschule für Technik, Wissenschaft und Kultur Leipzig (HTWK Leipzig)**

Prof. Dr. Alexander Grossmann

**KIT Scientific Publishing**

Christine Rhode

**Universitätsbibliothek Mannheim, Open Science Office**

Dr. Philipp Zumstein

**Universität Potsdam, Universitätsbibliothek**

Marco Winkler

**ZB MED – Informationszentrum Lebenswissenschaften**

Kyra Hennschen