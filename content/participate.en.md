---
title: "Join us making publishing easier"
date: 2021-02-07T18:58:57+01:00
type: single
draft: false
description: "Join us making publishing easier with OS-APS"
---

**OS-APS STEMO Advisory Board**

An advisory board has been established to accompany the project constructively and productively throughout its entire duration, as well as to prioritise and steer the software development in the interests of the publishing community: The **advisory board** is contacted on project questions and concerns. It also fulfills an advisory function in the strategic planning and design of the project.

The advisory board is made up of small and medium-sized publishers, university presses, academic libraries, externally funded projects and other committed players in the field of digital publishing and open science.

Interested? If you would like to support us within the framework of the advisory boards or outside of them, this is always possible. Just get in touch with us.

Contact: [mail@os-aps.de](mailto:mail@os-aps.de)

<div class="mt4"></div>

## **Members of the Scientific Advisory Board** {#sec:noq223umxt}

<div class="col-10 my4 justify-center mx-auto flex flex-wrap logo-wrapper">
    <img src="/images/partners/de_gruyter_logo.png" alt="de Gruyter" class="m2" />
    <img src="/images/partners/Logo_FID_Philosophie_cmyk-H.jpg" alt="FID Philosophie" class="m2" />
    <img src="/images/partners/Logo_Helmholtz.jpg" alt="Helmholtz Open Science Office" class="m2" />
    <img src="/images/partners/Sikorski_KIT.png" alt="KIT Scientific Publishing" class="m2" />
    <img src="/images/partners/Logo_Universitätsbibliothek_Mannheim.svg" alt="Universitätsbibliothek der Universität Mannheim" class="m2" />
    <img src="/images/partners/Uni_Potsdam_Logo.svg" alt="Universitätsbibliothek Potsdam" class="m2" />
</div>

**De Gruyter Brill**

Franziska Bühring

**USB Köln, FID Philosophie**

Yannik Hampf

**Helmholtz Open Science Office, Helmholtz-Gemeinschaft**

Lea Maria Ferguson

**Hochschule für Technik, Wissenschaft und Kultur Leipzig (HTWK Leipzig)**

Prof. Dr. Alexander Grossmann

**KIT Scientific Publishing**

Christine Rhode

**Universitätsbibliothek Mannheim, Open Science Office**

Dr. Philipp Zumstein

**Universität Potsdam, Universitätsbibliothek**

Marco Winkler

**ZB MED – Informationszentrum Lebenswissenschaften**

Kyra Hennschen
