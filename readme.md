# OS-APS Website

## Getting Started

Start a live Gitpod sesson via the Gitlab interface or prefixing `gitpod.io/#` to the repository URL.

## Development

Gitpod will run a command upon initialization to host the Website onto a server, which can be accessed by the URL provided by Gitpod.

You can also run the following command within Gitpod to start the server using:

```bash
hugo server -D -F --baseURL $(gp url 1313) --liveReloadPort=443 --appendPort=false --bind=0.0.0.0
```

## Contributing

To submit a new change:

1. Make your changes on a new git branch.
2. Push your branch to GitLab.
3. In GitLab, open a merge request to `website:main`.